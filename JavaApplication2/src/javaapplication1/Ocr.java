/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Harold
 */

import java.io.File;
import net.sourceforge.tess4j.*;


public class Ocr {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        File imageFile = new File("D:/Imagenes/pensamiento.png");
        ITesseract instance = new Tesseract();
        try{
            String result = instance.doOCR(imageFile);
            System.out.println(result);
        } catch (TesseractException e){
            System.err.println(e.getMessage());
        }
    }
    
    public String reconocer(String image){
        File imageFile = new File(image);
        ITesseract instance = new Tesseract();
        String result = "no read";
        try{
            result = instance.doOCR(imageFile);
            System.out.println(result);
        } catch (TesseractException e){
            System.err.println(e.getMessage());
        }
        return result;
    }
    
}
